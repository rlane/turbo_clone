# TurboClone
This rails engine is my simple clone of the turbo-rails gem.  It doesn't include any of the JS, and simply uses the technique that 
turbo-rails uses to pin the turbo libraries and provide helpers for use.

## Usage
Once you have installed with a rails app that is not using hotwire, you can use the turbo tags and 
helper methods just as you would normally use them in an app that has hotwire.

## Installation
Add this line to your application's Gemfile:

```ruby
gem "turbo_clone"
```

And then execute:
```bash
$ bundle
$ rails turbo_clone:install
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
